# Created 2019-08-15 Thu 15:10
#+TITLE: My Python tools
#+AUTHOR: Matthieu Leclair
#+export_file_name: README
#+startup: overview

These are my miscelaneous python tools not yet (or never to be) strucutrured in proper packages.
* Install
#+begin_src shell
  pip install git+ssh://git@gitlab.ethz.ch/leclairm/my-python-tools.git
#+end_src

* projection_tools

** Rotated pole coordinate transforms
adapted from:
- https://knowledge.safe.com/questions/43959/netcdf-rotated-coordinate-system-rlat-rlon.html
- https://knowledge.safe.com/storage/attachments/7810-rotated-grid-transform.py
  
* mpl_tools

** get_map_aspect_ratio
Derive the map aspect ratio of a cartopy projection
** grid_fig
Matplotlib already has this feature that does actually more but this is the way I do it
** pcolor_xy
Extend (x,y) for pcolormesh input
* math_tools

** prime_factors
** great_circle_distance
See https://www.movable-type.co.uk/scripts/latlong.html
