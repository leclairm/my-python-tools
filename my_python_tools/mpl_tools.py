import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interpn


def get_map_aspect_ratio(projection, map_extent=None, extent_proj=None):
    """Derive the map aspect ratio of a cartopy projection"""

    if map_extent is not None:
        fake_fig = plt.figure()
        ax = fake_fig.add_axes([0, 0, 1, 1], projection=projection)
        ax.set_extent(map_extent, crs=extent_proj)
        plt.close(fake_fig)
        ar = float(np.diff(ax.get_ylim()) / np.diff(ax.get_xlim()))
    else:
        ar = float(np.diff(projection.y_limits) / np.diff(projection.x_limits))

    return ar


def grid_fig(nx, ny, DX=10, DY=None, aspect_ratio=None, unit='CM',
             pad_in=None, ML=None, MR=None, MT=None, MB=None,
             pad_out=None, MLL=None, MRR=None, MTT=None, MBB=None,
             projection=None, map_extent=None, extent_proj=None,
             fig_kw={}, axes_kw={}):

    if unit == 'CM':
        unit2inch = 1.0/2.54
    elif unit == 'inch':
        unit2inch = 1.0
    else:
        raise NotImplementedError("Only 'CM' or 'inch' implemented "
                                  "for the 'unit' kwarg")

    # Compute aspect ratio
    if aspect_ratio is None:
        if projection is not None:
            aspect_ratio = get_map_aspect_ratio(projection,
                                                map_extent=map_extent,
                                                extent_proj=extent_proj)
        else:
            aspect_ratio = 1.0

    # Compute single panel height
    if DY is None:
        DY = DX * aspect_ratio

    # Compute margins
    if pad_in is not None:
        ML, MR, MT, MB = (pad_in/2, ) * 4
    else:
        if ML is None:
            ML = DX / 80
        if MR is None:
            MR = DX / 80
        if MT is None:
            MT = DX / 80
        if MB is None:
            MB = DX / 80

    if pad_out is not None:
        MLL, MRR, MTT, MBB = (pad_out, ) * 4
    else:
        if MLL is None:
            MLL = DX / 40
        if MRR is None:
            MRR = DX / 40
        if MTT is None:
            MTT = DX / 40
        if MBB is None:
            MBB = DX / 40

    X_SIZE = MLL - ML + nx * (ML+DX+MR) - MR + MRR
    Y_SIZE = MBB - MB + ny * (MB+DY+MT) - MT + MTT

    fig = plt.figure(figsize=[X_SIZE*unit2inch, Y_SIZE*unit2inch], **fig_kw)

    dx = DX / X_SIZE
    mll = MLL / X_SIZE
    ml = ML / X_SIZE
    mrr = MRR / X_SIZE
    mr = MR / X_SIZE

    dy = DY / Y_SIZE
    mbb = MBB / Y_SIZE
    mb = MB / Y_SIZE
    mtt = MTT / Y_SIZE
    mt = MT / Y_SIZE

    axes = [[None] * nx for ky in range(ny)]
    axes_kw['projection'] = projection
    for ky in range(ny):
        for kx in range(nx):
            x0 = mll + kx * (dx+mr+ml)
            y0 = 1.0 - (mtt + dy + ky * (mb+mt+dy))
            ax = fig.add_axes([x0, y0, dx, dy], **axes_kw)
            if map_extent is not None:
                ax.set_extent(map_extent, crs=extent_proj)
            axes[ky][kx] = ax

    return fig, axes, {'dx': dx, 'dy': dy,
                       'mll': mll, 'ml': ml, 'mr': mr, 'mrr': mrr,
                       'mbb': mbb, 'mb': mb, 'mt': mt, 'mtt': mtt}


def pcolor_xy(x, y, method="linear"):

    def _raise_shape_error():
        raise ValueError("x and y must either be of the same 2D shape "
                         "or both 1D")

    def _interp_1d(x):

        sz = x.size
        ii = (np.arange(sz),)
        II = np.arange(sz+1) - 0.5

        return interpn(ii, x, II, method=method,
                       bounds_error=False, fill_value=None)

    def _interp_2d(x):

        sh = x.shape
        ij = np.arange(sh[0]), np.arange(sh[1])
        sh_ext = np.array(sh, dtype=np.int) + 1
        IJ = (np.indices(sh_ext)-0.5).reshape(2, -1).T

        return interpn(ij, x, IJ, method=method,
                       bounds_error=False, fill_value=None).reshape(sh_ext)

    if x.ndim == 1:
        if y.ndim != 1:
            _raise_shape_error()
        else:
            return _interp_1d(x), _interp_1d(y)

    elif x.ndim == 2:
        if y.shape != x.shape:
            _raise_shape_error()
        else:
            return _interp_2d(x), _interp_2d(y)

    else:
        _raise_shape_error()


tango_colors = {
    'Aluminium1': '#EEEEEC',
    'Aluminium2': '#D3D7CF',
    'Aluminium3': '#BABDB6',
    'Aluminium4': '#888A85',
    'Aluminium5': '#555753',
    'Aluminium6': '#2E3436',
    'Butter1': '#FCE94F',
    'Butter2': '#EDD400',
    'Butter3': '#C4A000',
    'Chameleon1': '#8AE234',
    'Chameleon2': '#73D216',
    'Chameleon3': '#4E9A06',
    'Orange1': '#FCAF3E',
    'Orange2': '#F57900',
    'Orange3': '#CE5C00',
    'Chocolate1': '#E9B96E',
    'Chocolate2': '#C17D11',
    'Chocolate3': '#8F5902',
    'SkyBlue1': '#729FCF',
    'SkyBlue2': '#3465A4',
    'SkyBlue3': '#204A87',
    'Plum1': '#AD7FA8',
    'Plum2': '#75507B',
    'Plum3': '#5C3566',
    'ScarletRed1': '#EF2929',
    'ScarletRed2': '#CC0000',
    'ScarletRed3': '#A40000'
}
