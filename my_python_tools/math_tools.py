import math
import numpy as np


def prime_factors(n):

    prime_list = []

    k = 2
    K = int(math.sqrt(n))
    while (k <= K):
        power = 0
        while n % k == 0:
            power += 1
            n = n // k

        if power > 0:
            prime_list += [(k, power)]
            K = int(math.sqrt(n))

        k += 1

    if n > 2:
        prime_list += [(n, 1)]

    return prime_list


def great_circle_distance(P1, P2):
    """Compute great circle distance between lon/lat points on Earth

    P1 and P2 are the arrays of points between which the distance is
    computed and must be given in a (lon, lat) format.
    """

    R = 6371
    lda1, phi1 = np.deg2rad(P1[0,...]), np.deg2rad(P1[1,...])
    lda2, phi2 = np.deg2rad(P2[0,...]), np.deg2rad(P2[1,...])

    a = np.sin(0.5*(phi2-phi1))**2 \
        + np.cos(phi1) * np.cos(phi2) * np.sin(0.5*(lda2-lda1))**2

    return 2.0 * R * np.arctan2(np.sqrt(a), np.sqrt(1-a))
