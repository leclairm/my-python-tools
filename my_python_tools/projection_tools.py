import numpy as np


deg2rad = np.pi / 180.0
rad2deg = 180.0 / np.pi


def rot2geo(grid_in, rotated_north_pole):

    # rotated meshgrid
    rlon, rlat = np.meshgrid(grid_in[0] * deg2rad, grid_in[1] * deg2rad)

    # Rotation angles
    a1 = rotated_north_pole[1] * deg2rad - 0.5 * np.pi  # around y-axis
    a2 = np.pi - rotated_north_pole[0] * deg2rad        # around z-axis
    c1, s1 = np.cos(a1), np.sin(a1)
    c2, s2 = np.cos(a2), np.sin(a2)

    # Convert from spherical to cartesian coordinates
    x = np.cos(rlon) * np.cos(rlat)
    y = np.sin(rlon) * np.cos(rlat)
    z = np.sin(rlat)

    # Rotate
    x_new = c1 * c2 * x + s2 * y + s1 * c2 * z
    y_new = -c1 * s2 * x + c2 * y - s1 * s2 * z
    z_new = -s1 * x + c1 * z

    # Convert cartesian back to spherical coordinates
    lon = np.arctan2(y_new, x_new)
    lat = np.arcsin(z_new)

    # Return geographical (lon, lat) in degrees
    return (lon * rad2deg, lat * rad2deg)


def geo2rot(grid_in, rotated_north_pole):

    # geographical meshgrid
    lon, lat = np.meshgrid(grid_in[0] * deg2rad, grid_in[1] * deg2rad)

    # Rotation angles
    a1 = 0.5 * np.pi - rotated_north_pole[1] * deg2rad   # around y-axis
    a2 = np.pi + rotated_north_pole[0] * deg2rad         # around z-axis
    c1, s1 = np.cos(a1), np.sin(a1)
    c2, s2 = np.cos(a2), np.sin(a2)

    # Convert from spherical to cartesian coordinates
    x = np.cos(lon) * np.cos(lat)
    y = np.sin(lon) * np.cos(lat)
    z = np.sin(lat)

    # Rotate
    x_new = c1 * c2 * x + c1 * s2 * y + s1 * z
    y_new = -s2 * x + c2 * y
    z_new = -s1 * c2 * x - s1 * s2 * y + c1 * z

    # Convert cartesian back to spherical coordinates
    rlon = np.arctan2(y_new, x_new)
    rlat = np.arcsin(z_new)

    # Return rotated (rlon, rlat) in degrees
    return (rlon * rad2deg, rlat * rad2deg)
